//
//  server.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 19/03/14.
//
//

#include "lib.h"


int
answerToClient(int client)
{
    FILE *fp = NULL;
    DH *myDH;
    BIGNUM *clientPublicKey;
    struct crypto msg;
    int ret = -1, i = 0, digest_len = 20, key_len = 0, codes = 0;
    unsigned char *key;
    char *digest, mydigest[digest_len];
	
    //Genero i parametri
    do {
	myDH = DH_generate_parameters(512,DH_GENERATOR_5,NULL,NULL);
    }
    while(DH_check(myDH,&codes) == 0);
    printf("\nGeneration of parameters p and g\n");
	
    //Allocazione dello spazio per la chiave pubblica del client
    clientPublicKey = BN_new();
	
		
    //Alloco coppia chiavi
    myDH->pub_key = BN_new();
    myDH->priv_key = BN_new();
	
    //Genero la coppia di chiavi
    if(DH_generate_key(myDH) != 1)
    {
    	perror("DH_generate_key error!");
    	exit(EXIT_FAILURE);
    }
	
    printf("\nGeneration of key pair\n");
	
    //Invio parametri e chiave pubblica al client
    sendBIGNUM(client,myDH->p,"Parameter p sent successfully");
    sendBIGNUM(client,myDH->g,"Parameter g sent successfully");
    sendBIGNUM(client,myDH->pub_key,"Public key sent successfully");
	
    //Ricevo la chiave publica dal client
    receiveBIGNUM(client,&clientPublicKey,"Client public key received successfully");

    //Calcolo la lunghezza della chiave e la alloco
    key_len = DH_size(myDH);
    key = (unsigned char *) calloc(key_len, sizeof(char));
	
    //Genero la chiave di "sessione"
    key_len = DH_compute_key(key, clientPublicKey, myDH);
	
    //Inserisco la chiave nella struttura
    stpncpy(msg.k,(char *)key,8);
    //enterPassword(msg.k);
	
    //Ricevo il cipher text dal client
    receiveMessage(client, &msg.ct, &msg.ct_len);
	
    printf("\nDimensione del ciphertext: %u\n\n",msg.ct_len);
    
    //decifro il messaggio
    my_decrypt(&msg);
    
    //EVP_CIPHER_CTX_cleanup(ctx);
    
    // salvo il digest ricevuto
    digest = &msg.pt[msg.pt_len - digest_len];
    msg.pt_len -= digest_len;    
    
    //Stampo il digest ricevuto
    printf("\nDigest SHA-1 ricevuto con il messaggio:\n");
	
    for (i = 0; i < digest_len ; i++)
	printbyte(digest[i]);
    
    printf("\n\n");
    
    // calcolo il digest sul testo ricevuto
    hash_sha1(&msg,mydigest);

    //Confronto i digest
    ret = memcmp(digest,mydigest,digest_len);
    
    if(ret == 0)
    {
	printf("Message verified!\n");
        
        //Stampo il messaggio a video
        //printf("\nPlain text:\n%s\n\n", msg.pt);
        
        //Salvo il messaggio nel file "received-data.txt"
        fp = fopen("received-data.txt", "w");
        if (fp == NULL)
        {
            perror("Opening file error\n");
            exit(EXIT_FAILURE);
        }
        
        fprintf(fp, "%s", msg.pt);
        fclose(fp);
    }
    else
	printf("Message NOT verified!\n\n");
    
    //Deallocazione DH e client public key
	BN_free(clientPublicKey);
	DH_free(myDH);
    
    //Deallocazione messaggio e chiave di cifratura
    free(key);
    free(msg.pt);
    free(msg.ct);
	
    return 0;
}


int
main(void)
{
	//allocazione delle strutture dati necessarie
	struct sockaddr_in my_addr, cl_addr;
	int ret, len, sk, cn_sk;
	
	//Verbose
	printf("Avvio del server\n");
	
	//creazione del socket di ascolto
	sk = socket(AF_INET, SOCK_STREAM, 0);
	
	if(sk == -1)
	{
		perror("Errore nella creazione del socket");
		return 0;
	}
	else
		printf("Socket di ascolto creato correttamente\n");
	
	//inizializzazione delle strutture dati
	memset(&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	my_addr.sin_port = htons(1234);
	
	//bind del socket
	ret = bind(sk, (SA *) &my_addr, sizeof(my_addr));
	
	if(ret == -1)
	{
		perror("Errore nell'esecuzione della bind del socket");
		return 0;
	}
	else
		printf("Bind del socket eseguita correttamente\n");
    
	//messa in ascolto del server sul socket
	ret = listen(sk, 10);
	
	if(ret == -1)
	{
		perror("Errore nella messa in ascolto del server");
		return 0;
	}
	else
		printf("Server messo in ascolto sul socket correttamente\n");
	
	//dimensioni della struttura dove viene salvato l'ind del client
	len = sizeof(cl_addr);
	
	//ciclo in cui il server accetta connessioni in ingresso e le gestisce
	for(;;)
	{
        printf("\nWaiting for client message...\n\n");
        
		//accettazione delle connessioni ingresso
		cn_sk = accept(sk, (SA *) &cl_addr, (socklen_t *) &len);
		
		if(cn_sk == -1)
		{
			perror("Errore nell'accettazione di una richiesta");
			return 0;
		}
		else
			printf("Richiesta accettata correttamente\n");
		
		//gestione delle richieste
		answerToClient(cn_sk);
	}
	
	//Chiusura del socket di ascolto
	//Questo codice non verrà mai eseguito dato che si trova
	//dopo un ciclo infinito
	
	printf("\nChiusura del server\n");
	close(sk);
	return 0;
}



