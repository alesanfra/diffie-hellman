//
//  client.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 19/03/14.
//
//

#include "lib.h"
#define FILE_BLOCK_LEN 256

int
main(int argc, char* argv[])
{
    FILE *fp = NULL;
    DH* myDH;
    BIGNUM* serverPublicKey;
    struct crypto msg;
    unsigned int file_len = FILE_BLOCK_LEN;
    int digest_len = 20, i = 0, server = 0, key_len = 0;
    unsigned char* key;
    
    //Controllo degli argomenti
    if(argc != 2)
    {
	printf("Usage: client <file>\n");
	exit(EXIT_FAILURE);
    }
	  
    //Connetto al server  
    server = connectToServer("127.0.0.1",1234);

    
    //Alloco le strutture dati necessarie per ricevere la chiave
    myDH = DH_new();
    //serverPublicKey = BN_new();
        
    //Ricevo i parametri
    //receiveParameters(server,myDH);
    receiveBIGNUM(server,&myDH->p,"Parameter p received successfully");
    receiveBIGNUM(server,&myDH->g,"Parameter g received successfully");
        
    //Alloco coppia chiavi
    myDH->pub_key = BN_new();
    myDH->priv_key = BN_new();
    
    //Genero la mia coppia di chiavi
    if(DH_generate_key(myDH) != 1)
    {
    	perror("DH_generate_key error!");
    	exit(EXIT_FAILURE);
    }
    
    printf("\nGeneration of key pair\n");
    
    //Ricevo chiave pubblica dal server
    //receivePublicKey(server,serverPublicKey);
    receiveBIGNUM(server,&serverPublicKey,"Server public key received successfully");
    
    //Invio la chiave pubblica al server
    //sendPublicKey(server,myDH->pub_key);
    sendBIGNUM(server,myDH->pub_key,"Public key sent successfully");
    
    //Calcolo la lunghezza della chiave e la alloco
    key_len = DH_size(myDH);
    key = (unsigned char *) calloc(key_len, sizeof(char));
    
    //Calcolo la chiave
    key_len = DH_compute_key(key, serverPublicKey, myDH);
    
    
    //Azzero la struttura
    memset(&msg,0,sizeof(msg));
    
    //Inserisco la chiave nella struttura
    stpncpy(msg.k,(char *)key,8);
    //enterPassword(msg.k);
    
    
    
    //Allocazione dello spazio per leggere il contenuto del file
    msg.pt = (char*) calloc(file_len,sizeof(char));
    
    //Apertura file
    fp = fopen(argv[1],"r");
    
    //Controllo se l'apertura è andata a buon fine
    if(fp == NULL)
    {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }
    
    //Leggo tutti i byte fino a alla fine del file
    for(i=0; (msg.pt[i] = fgetc(fp)) != EOF; i++)
    {
      if(i == file_len-1)
      {
	file_len += FILE_BLOCK_LEN;
	msg.pt = (char*) realloc(msg.pt,file_len);
      }
    }
    
    //Fine stringa al posto di EOF
    msg.pt[i] = '\0';
    
    //Chiudo il file
    fclose(fp);
    
    /* Output of the original message */
    printf("\nOriginal message:\n%s\n",msg.pt);
    
    //Calcolo della dimensione della stringa
    msg.pt_len = strlen(msg.pt)+1;
    
    //Calcolo l'hash e lo concateno al plain text
    hash_sha1(&msg,&msg.pt[msg.pt_len]);
    
    //Aggiorno la dimesione del plain text
    msg.pt_len += digest_len;

    //Rilascio dello spazio non utilizzato nel plaintext
    msg.pt = (char *) realloc(msg.pt, (size_t)msg.pt_len);
	
    //Cifratura del messaggio + hash
    my_encrypt(&msg);
    
    //Invio file
    sendMessage(server, msg.ct, msg.ct_len);

    //Deallocazione DH e server public key
    BN_free(serverPublicKey);
    DH_free(myDH);
    
    //Deallocazione messaggio e chiusura app
    free(key);
    free(msg.pt);
    free(msg.ct);
    
    printf("\nClosing client...\n");
    
    return 0;
}